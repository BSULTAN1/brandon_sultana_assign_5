package fileVisitors.driver;

import java.util.ArrayList;

public class Driver {
    
    public static MyLogger logger = new MyLogger();
    
    /** Driver functione
    * Perform error checking on the command line arguments
    * Create instances of Visitor and Tree
    * Create an instance of PopulateVisitor and visit it
    * Create an instance of PalindromeHighlight and visit it
    * Create an instance of PrimeLength and visit it
    * Create an instance of PrintTree and visit it
    * Turn the array list int one string
    * Use that string and an instance of Results to output the results to the appropriate destination
    * @param (String[]) args - The command line arguments
    * @return (void)
    */
    public static void main(String[] args) {
        //Check number of arguments
        if(args.length != 3) {
            System.err.println("Usage: <Input File> <Output File> <Debug Value>");
            System.exit(1);
        }
        
        //Check if each argument is valid
        if(args[0].equals("${arg0}") || args[1].equals("${arg1}") || args[2].equals("${arg2}")) {
            System.err.println("Usage: <Input File> <Output File> <Debug Value>");
            System.exit(1);
        }
        
        //Parse debugLevel
        int debugLevel = 0;
        try{
            debugLevel = Integer.parseInt(args[2]);
        }
        catch(NumberFormatException e) {
            System.err.println("Argument 3 must be an int");
            System.exit(1);
        }
        if(debugLevel < 0 || debugLevel > 4) {
            System.err.println("Argument 3 must be between 0 and 4");
            System.exit(1);
        }
        
        //Set Debug Level
        logger.setDebugValue(debugLevel);
        
        //Create tree and visitor
        TreeBuilder tree = new TreeBuilder();
        Visitor visitor = new Visitor();
        
        //Create objects to be visited
        PopulateVisitor populateVisitor = new PopulateVisitor(args[0], tree);
        PalindromeHighlight palindromeHighlight = new PalindromeHighlight(tree);
        PrimeLength primeLength = new PrimeLength(tree);
        PrintTree printTree = new PrintTree(tree);
        
        //Visit each object
        visitor.visit(populateVisitor);
        visitor.visit(palindromeHighlight);
        visitor.visit(primeLength);
        visitor.visit(printTree);
        
        //Turn the results from printTree into a string
        String data = "";
        for(int i = 0; i < tree.getWords().size(); i++) {
            if(i == tree.getWords().size()-1) {
                data = data + tree.getWords().get(i);
            }
            else {  
                data = data + tree.getWords().get(i) + "\n";
            }
        }
        
        //Output the string
        Results results = new Results(args[1]);
        results.writeToFile(data);
        results.writeToScreen(data);
    }
}