package fileVisitors.driver;

public interface FileDisplayInterface {
    public void writeToFile(String output);
}