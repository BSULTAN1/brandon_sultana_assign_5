package fileVisitors.driver;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileProcessor {
    
    private String inputFile;
    
    /** Constructor for FileProcessor
    * @param (String) inFile - The input file to read from
    * @return (void)
    */
    public FileProcessor(String inFile) {
        Driver.logger.writeMessage("Instance of FileProcessor created.", MyLogger.DebugLevel.CONSTRUCTOR);
        inputFile = inFile;
    }
    
    /** Reads a file and inserts each line into the tree
    * First sets up a BufferedReader using the file name from the constructor
    * For each line of the file, call insertNode from the tree using the line to insert it into the tree
    * Perform error checks
    * @param (TreeBuilder) tree - the tree to be worked on
    * @return (void)
    */
    public void readFile(TreeBuilder tree) {
        File inFile = new File(inputFile);
        BufferedReader read = null;
        
        try {
            read = new BufferedReader(new FileReader(inFile));
            String line;
            while((line = read.readLine()) != null) {
                
                tree.insertNode(line);
                
            }
        }
        //Catch File Not Found
        catch(FileNotFoundException e) {
            System.err.println("ERROR: Unable to open input file");
            System.exit(0);
        }
        //Catch IO Exception
        catch(IOException e) {
            System.err.println("ERROR: IO Exception");
            System.exit(0);
        }
        finally {
            try{
                read.close();
            }
            catch(IOException e) {
                System.err.println("Error: IO Exception");
                System.exit(0);
            }
        }
    }
}