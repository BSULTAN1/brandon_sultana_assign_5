package fileVisitors.driver;

public class MyLogger {
    
    /*DEBUG_VALUE=4 [Print to stdout everytime a constructor is called]
      DEBUG_VALUE=3 [Print every time a Palindrome or Prime member is found]
      DEBUG_VALUE=2 [Print every time a Word is added to the Tree]
      DEBUG_VALUE=1 [Print the Results to Stdout]
      DEBUG_VALUE=0 [No output should be printed from the applicatio to stdout. It is ok to write to the output file though" ]
    */

    public static enum DebugLevel {RELEASE, RESULT, ADD, PALINDROME_PRIME, CONSTRUCTOR};

    private static DebugLevel debugLevel;
    //private static Results results = new Results();

    /** Constructor for MyLogger
    * @param (void)
    * @return (void)
    */
    public MyLogger() {
        Driver.logger.writeMessage("Instance of MyLogger created.", MyLogger.DebugLevel.CONSTRUCTOR);
    }
    
    /** Mutator for debugValue
    * Sets the debugValue to the corresponding enum based on an int
    * Then calls functions to output these trees to three separate output files
    * @param (int) levelIn - The debugLevel represented by an int, received from command line
    * @return (void)
    */
    public static void setDebugValue (int levelIn) {
        switch (levelIn) {
            case 4: debugLevel = DebugLevel.CONSTRUCTOR; break;
            case 3: debugLevel = DebugLevel.PALINDROME_PRIME; break;
            case 2: debugLevel = DebugLevel.ADD; break;
            case 1: debugLevel = DebugLevel.RESULT; break;
            case 0: debugLevel = DebugLevel.RELEASE; break;
        }
    }

    /** Mutator for debugValue
    * Sets the debugValue based on an enum
    * @param (DebugValue) levelIn - An enum representing one of five debugLevels for the program
    * @return (void)
    */
    public static void setDebugValue (DebugLevel levelIn) {
        debugLevel = levelIn;
    }

    /** Prints a Debug Message
    * Checks if the debugLevel received matches the current debugLevel
    * If it does, prints the message
    * @param (String) message - The debug message to be printed
    * @param(DebugLevel) levelIn - the debugLevel of the message
    * @return (void)
    */
    public static void writeMessage (String  message, DebugLevel levelIn ) {
        if (levelIn == debugLevel) {
            System.out.println(message);
        }
    }

    /**
	 * @return String - string representation of the Logger
	 */
    public String toString() {
	   return "Debug Level is " + debugLevel;
    }
    
}