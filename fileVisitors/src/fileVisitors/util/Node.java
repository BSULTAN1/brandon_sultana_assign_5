package fileVisitors.driver;

public class Node {  
    private String word;
    private Node leftChild;
    private Node rightChild;
    
    /** Constructor for Node
    * @param (String w) - The word stored in the node
    * @return (void)
    */
    public Node(String w) {
        Driver.logger.writeMessage("Instance of Node created.", MyLogger.DebugLevel.CONSTRUCTOR);
        word = w;
        leftChild = null;
        rightChild = null;
    }
    
    /** Accessor for word
    * @param (void)
    * @return (void) word - The word in the Node
    */
    public String getWord() {
        return word;
    }
    
    /** Mutator for word
    * @param (String) newWord - The new word to be stored in the Node
    * @return (void)
    */
    public void setWord(String newWord) {
        word = newWord;
    }
    
    /** Accessor for leftChild
    * @param (void)
    * @return (void) leftChild - The left child of the node
    */
    public Node getLeft() {
        return leftChild;
    }
    
    /** Mutator for leftChild
    * @param (Node) newLeft - the new left child of the node
    * @return (void)
    */
    public void setLeft(Node newLeft) {
        leftChild = newLeft;
    }
    
    /** Accessor for rightChild
    * @param (void)
    * @return (void) rightChild - The right child of the node
    */
    public Node getRight() {
        return rightChild;
    }
   
    /** Mutator for rightChild
    * @param (Node) newRight - the new right child of the node
    * @return (void)
    */
    public void setRight(Node newRight) {
        rightChild = newRight;
    }
    
    /** String representation of the node
    * @param (void)
    * @return (String) - A string containing the word and its number of occurences
    */
    public String toString() {
        return "Word: " + word;
    }
}