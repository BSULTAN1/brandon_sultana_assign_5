package fileVisitors.driver;

import java.lang.StringBuilder;
import java.util.ArrayList;

public class TreeBuilder {
    
    //Parameters, Root of the Tree, and array list of nodes listed in ascending order
    private Node root;
    private ArrayList<String> words = new ArrayList<String>();
    
    /** Empty Constructor for TreeBuilder
    * @param (void)
    * @return (void)
    */
    public TreeBuilder() {
        Driver.logger.writeMessage("Instance of Tree Builder created.", MyLogger.DebugLevel.CONSTRUCTOR);
        root = null;
    }
    
    /** Accessor for Root
    * @param (void)
    * @return (Node) root
    */
    public Node getRoot() {
        return root;
    }
    
    /** Accessor for Words
    * @param (void)
    * @return (ArrayList<String>) words
    */
    public ArrayList<String> getWords() {
        return words;
    }
    
    /** Inserts a node into the tree
    * First checks if a root exists for the tree
    * If no root exists, create one and return
    * Otherwise, traverse the tree downward
    * Traverse Left or Right depending on the value of the word and the current node
    * Upon reaching an empty node, fill it with the word and break, then return
    * @param (String) word - The word to be inserted into the tree
    * @return (void)
    */
    public void insertNode(String word) {
        Node current = root;
        if(root == null) {
            Driver.logger.writeMessage(word + " inserted", MyLogger.DebugLevel.ADD);
            Node node = new Node(word);
            root = node;
            return;
        }
        
        while(current != null) {
            if(word.equals(current.getWord())) {
                break;
            }
            else if(word.compareTo(current.getWord()) < 0) {
                if(current.getLeft() == null) {
                    Driver.logger.writeMessage(word + " inserted", MyLogger.DebugLevel.ADD);
                    Node node = new Node(word);
                    current.setLeft(node);
                    break;
                }
                else {
                    current = current.getLeft();
                }
            }
            else {
                if(current.getRight() == null) {
                    Driver.logger.writeMessage(word + " inserted", MyLogger.DebugLevel.ADD);
                    Node node = new Node(word);
                    current.setRight(node);
                    break;
                }
                else {
                    current = current.getRight();
                }
            }
        }
    }
}