package fileVisitors.driver;

public class PalindromeHighlight {
    
    private TreeBuilder tree;
    
    /** Constructor for PalindromeHighlight
    * @param (TreeBuilder) t - The tree to be worked on
    * @return (void)
    */
    public PalindromeHighlight(TreeBuilder t) {
        Driver.logger.writeMessage("Instance of PalindromeHighlight created.", MyLogger.DebugLevel.CONSTRUCTOR);
        tree = t;
    }
    
    /** Calls traverse using the root of the tree
    * @param (void)
    * @return (void)
    */
    public void highlight() {
        traverse(tree.getRoot());
    }
    
    /** Traverses the tree to highlight palindromes
    * Starts at the root of the tree
    * Traverse Left
    * If the word in the current node is the same as the word reversed, capitalize the word to signify a palindrome
    * Traverse Right
    * @param (Node) current - Initially the root of the node
    * @return (void)
    */
    public void traverse(Node current) {
        if(current == null) {
            return;
        }
        traverse(current.getLeft());
            
        if(current.getWord().equals((new StringBuilder(current.getWord()).reverse()).toString())) {
            Driver.logger.writeMessage(current.getWord() + " is a palindrome", MyLogger.DebugLevel.PALINDROME_PRIME);
            current.setWord(current.getWord().toUpperCase());
        }
        traverse(current.getRight());
    }
}