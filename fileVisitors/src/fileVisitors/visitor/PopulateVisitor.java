package fileVisitors.driver;

public class PopulateVisitor {
    
    private String inputFile;
    private TreeBuilder tree;
    
    /** Constructor for PopulateVisitor
    * @param (String in) - The name of the input file
    * @param (TreeBuilder) t - the tree to be worked on
    * @return (void)
    */
    public PopulateVisitor(String in, TreeBuilder t) {
        Driver.logger.writeMessage("Instance of PopulateVisitor created.", MyLogger.DebugLevel.CONSTRUCTOR);
        inputFile = in;
        tree = t;
    }
    
    /** Creates a fileProcessor and calls it to read the file
    * @param (void)
    * @return (void)
    */
    public void populate() {
        FileProcessor fp = new FileProcessor(inputFile);
        fp.readFile(tree);
    }
}