package fileVisitors.driver;

public class PrimeLength {
    
    private TreeBuilder tree;
    
    /** Constructor for PrimeLength
    * @param (TreeBuilder) t - The tree to be worked on
    * @return (void)
    */
    public PrimeLength(TreeBuilder t) {
        Driver.logger.writeMessage("Instance of PrimeLength created.", MyLogger.DebugLevel.CONSTRUCTOR);
        tree = t;
    }
    
    /** Calls a function to traverse the tree using the trees root
    * @param (void)
    * @return (void)
    */
    public void markPrime() {
        traverse(tree.getRoot());
    }
    
    /** Traverses the tree and highlights nodes with a word of prime length
    * Starts at the root of the tree
    * Traverse Left
    * Loops through all numbers from 2 to the length of the word
    * If the length of the word is not divisible by any of those numbers, it has a prime length
    * Write '-PRIME' at the end of the word to signify it is prime
    * Traverse Right
    * @param (Node) current - Initially the root of the node
    * @return (void)
    */
    public void traverse(Node current) {
        if(current == null) {
            return;
        }
        traverse(current.getLeft());
        
        boolean prime = true;
        for(int i = 2; i < current.getWord().length(); i++) {
            if(current.getWord().length()%i == 0) {
                prime = false;
                break;
            }
        }
        if(prime & current.getWord().length() >= 2) {
            Driver.logger.writeMessage(current.getWord() + " has a prime length", MyLogger.DebugLevel.PALINDROME_PRIME);
            current.setWord(current.getWord() + "-PRIME");
        }
        
        traverse(current.getRight());
    }
}