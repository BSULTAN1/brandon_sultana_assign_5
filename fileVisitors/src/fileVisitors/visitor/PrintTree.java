package fileVisitors.driver;

public class PrintTree{
    
    private TreeBuilder tree;
    
    /** Constructor for PrintTree
    * @param (TreeBuilder) t - The tree to be worked on
    * @return (void)
    */
    public PrintTree(TreeBuilder t) {
        Driver.logger.writeMessage("Instance of PrintTree created.", MyLogger.DebugLevel.CONSTRUCTOR);
        tree = t;
    }
    
    /** Calls a function to traverse the tree using the root of the tree
    * @param (void)
    * @return (void)
    */
    public void print() {
        traverse(tree.getRoot());
    }
    
    /** Traverses the tree and adds each word to an arraylist for later printing using results
    * Traverse Left, to ensure nodes are in ascending order
    * Add the word in the node to the array lest
    * Traverse Right
    * @param (Node) current - Initially the root of the node
    * @return (void)
    */
    public void traverse(Node current) {
        if(current == null) {
            return;
        }
        traverse(current.getLeft());
        
        tree.getWords().add(current.getWord());
        
        traverse(current.getRight());
        
    }
}