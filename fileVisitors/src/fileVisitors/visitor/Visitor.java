package fileVisitors.driver;

public class Visitor implements VisitorI {
  
    /** Constructor for Visitor
    * @param (void)
    * @return (void)
    */
    public Visitor() {
        Driver.logger.writeMessage("Instance of Visitor created.", MyLogger.DebugLevel.CONSTRUCTOR);
    }
    
    /** Calls a function in PopulateVisitor to implement logic
    * @param (PopulateVisitor) pv - Instance of PopulateVisitor to be visited
    * @return (void)
    */
    public void visit(PopulateVisitor pv) {
        pv.populate();
    }

    /** Calls a function in PalindromeHighlight to implement logic
    * @param (PalindromeHighlight) ph - Instance of PalindromeHighlight to be visited
    * @return (void)
    */
    public void visit(PalindromeHighlight ph) {
        ph.highlight();
    }
    
    /** Calls a function in PrimeLength to implement logic
    * @param (PrimeLength) pl - Instance of PrimeLength to be visited
    * @return (void)
    */
    public void visit(PrimeLength pl) {
        pl.markPrime();
    }
    
    /** Calls a function in PrintTree to implement logic
    * @param (PrintTree) pt - Instance of PrintTree to be visited
    * @return (void)
    */
    public void visit(PrintTree pt) {
        pt.print();
    }
}