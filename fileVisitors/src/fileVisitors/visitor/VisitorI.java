package fileVisitors.driver;

public interface VisitorI {
    public void visit(PopulateVisitor pv);
    public void visit(PalindromeHighlight ph);
    public void visit(PrimeLength pl);
    public void visit(PrintTree pt);
    
}